#include <stdio.h>
#include <math.h>

struct point
{
    int x;
    int y;
    
};
typedef struct point Point;
struct point input()
{
    Point a;
    printf("enter a point\n");
    scanf("%d %d",&a.x,&a.y);
    return a;
}
float compute(Point o,Point e)
{
    float d;
    d=(float)sqrt(pow((o.x-e.x),2)+pow((e.y-o.y),2));
    return d;
}
void output(Point o,Point e,float r)
{
    printf("distance between %d-%d %d-%d is %f",o.x,e.x,o.y,e.y,r);
}
int main()
{
    Point p;
    Point q;
    float r;
    p=input();
    q=input();
    r=compute(p,q);
    output(p,q,r);
    return 0;
    
}